import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image} from 'react-native';
import {FireBase} from './src/components/FireBase';
import Home from './src/components/Home/Home';
import Favoris from './src/components/Favoris/Favoris';
import Panier from './src/components/Panier/Panier';
import Tag from './src/components/Tag/Tag';
//import Register from './src/components/Register/Register';
import Recette from './src/components/Recette/Recette';
import Icon from 'react-native-ionicons'
import {createSwitchNavigator, createAppContainer, createStackNavigator, createBottomTabNavigator} from 'react-navigation';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});


//const AppStack = createStackNavigator(
//  { Welcome: WelcomeScreen,
//     Other: OtherScreen
//  },
//  {
//    headerMode: 'none',
//    navigationOptions: {
//      headerVisible: false,
//    }
//   });

const AcueilStack = createStackNavigator(
  {
    Home: { screen: Home },
    Recette: { screen: Recette }
  },
  {
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    }
   });


   const RecetteTabNavigator = createBottomTabNavigator(
     {
       Acueil: {
           screen: AcueilStack,
           navigationOptions: {
             tabBarIcon: ({ focused,tintColor }) => (
                focused ? <Image
                     source={require('./src/images/favorite-heart-button.png')}
                     style={[styles.icon, {tintColor: '#ED4C67'}]}
                   />
                  :
                  <Image
                       source={require('./src/images/favorite-heart-button.png')}
                       style={[styles.icon, {tintColor: 'black'}]}
                     />
              ),
           }
       },
       Favoris: {
           screen: Favoris,
           navigationOptions: {
             tabBarIcon: ({ focused,tintColor }) => (
                focused ? <Image
                     source={require('./src/images/favorite-heart-button.png')}
                     style={[styles.icon, {tintColor: '#ED4C67'}]}
                   />
                  :
                  <Image
                       source={require('./src/images/favorite-heart-button.png')}
                       style={[styles.icon, {tintColor: 'black'}]}
                     />
              ),
           }
       },
       Panier: {
           screen: Panier,
           navigationOptions: {
             tabBarIcon: ({ focused,tintColor }) => (
                focused ? <Image
                     source={require('./src/images/favorite-heart-button.png')}
                     style={[styles.icon, {tintColor: '#ED4C67'}]}
                   />
                  :
                  <Image
                       source={require('./src/images/favorite-heart-button.png')}
                       style={[styles.icon, {tintColor: 'black'}]}
                     />
              ),
           }
       },
       Tag: {
           screen: Tag,
           navigationOptions: {
             tabBarIcon: ({ focused,tintColor }) => (
                focused ? <Image
                     source={require('./src/images/tag.png')}
                     style={[styles.icon, {tintColor: '#ED4C67'}]}
                   />
                  :
                  <Image
                       source={require('./src/images/tag.png')}
                       style={[styles.icon, {tintColor: 'black'}]}
                     />
              ),
           }
       }
     },{
     tabBarOptions : {
       style: {
         backgroundColor: '#F8E3B4',
       }
     }
   }
   )

const AppTabNavigator = createBottomTabNavigator(
  {
    Acueil: {
        screen: AcueilStack,
        navigationOptions: {
          tabBarIcon: ({ focused,tintColor }) => (
             focused ? <Image
                  source={require('./src/images/home.png')}
                  style={[styles.icon, {tintColor: '#ED4C67'}]}
                />
               :
               <Image
                    source={require('./src/images/home.png')}
                    style={[styles.icon, {tintColor: 'black'}]}
                  />
           ),
        }
    },
    Favoris: {
        screen: Favoris,
        navigationOptions: {
          tabBarIcon: ({ focused,tintColor }) => (
             focused ? <Image
                  source={require('./src/images/favorite-heart-button.png')}
                  style={[styles.icon, {tintColor: '#ED4C67'}]}
                />
               :
               <Image
                    source={require('./src/images/favorite-heart-button.png')}
                    style={[styles.icon, {tintColor: 'black'}]}
                  />
           ),
        }
    },
    Panier: {
        screen: Panier,
        navigationOptions: {
          tabBarIcon: ({ focused,tintColor }) => (
             focused ? <Image
                  source={require('./src/images/shopping-cart-black-shape.png')}
                  style={[styles.icon, {tintColor: '#ED4C67'}]}
                />
               :
               <Image
                    source={require('./src/images/shopping-cart-black-shape.png')}
                    style={[styles.icon, {tintColor: 'black'}]}
                  />
           ),
        }
    },
    Tag: {
        screen: Tag,
        navigationOptions: {
          tabBarIcon: ({ focused,tintColor }) => (
             focused ? <Image
                  source={require('./src/images/tag.png')}
                  style={[styles.icon, {tintColor: '#ED4C67'}]}
                />
               :
               <Image
                    source={require('./src/images/tag.png')}
                    style={[styles.icon, {tintColor: 'black'}]}
                  />
           ),
        }
    }
  },{
  tabBarOptions : {
    style: {
      backgroundColor: '#F8E3B4',
    }
  }
}
)

const  AppSwichtNavigator = createSwitchNavigator({
  App:AppTabNavigator,
});

const AppContainer = createAppContainer(AppSwichtNavigator);


export default class App extends Component {
  render() {
      return (
        <AppContainer/>
      );
  }
}

const IconBar = () => (
  <View>
    <Icon ios="ios-add" android="md-add" />
  </View>
)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
