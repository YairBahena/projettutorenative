Commandes

   Pre-requis:
     Android Studio:
        - Android SDK
        - Android SDK Platform
        - Performance (Intel ® HAXM)
        - Android Virtual Device
      AndroidSDK Settings:
        - Android SDK Platform 28
        - Intel x86 Atom_64 System Image or Google APIs Intel x86 Atom System Image


   Demarrer app en mode developpement:
     -  ouvrir AVD(Android Virtual Device) depuis android stuido
     $  react-native run-android

   Log
     $ react-native log-android:
