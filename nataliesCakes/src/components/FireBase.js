import * as firebase from "firebase";

class FireBase {
  static init() {
    if (!firebase.apps.length) {
      firebase.initializeApp({
        apiKey: "AIzaSyBVMnkKLf0-GBQRsSWvBWAmgpnF79C3H4U",
        authDomain: "lp-wmce-projet-tutore.firebaseapp.com",
        databaseURL: "https://lp-wmce-projet-tutore.firebaseio.com",
        projectId: "lp-wmce-projet-tutore",
        storageBucket: "lp-wmce-projet-tutore.appspot.com",
        messagingSenderId: "300951782730"
      });
    }

  }
}

module.exports = FireBase
