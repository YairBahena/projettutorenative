import { SearchBar } from 'react-native-elements';
import React, {Component} from 'react';

export default class Search extends React.Component {
  state = {
    search: '',
  };

  updateSearch = search => {
    this.setState({ search });
  };

  render() {
    const { search } = this.state;
    return (
      <SearchBar
        placeholder="Tapez ici..."
        onChangeText={this.updateSearch}
        value={search}
        containerStyle={{backgroundColor: '#ED4C67'}}
        inputStyle={{backgroundColor: 'white'},{color:'black'}}
        inputContainerStyle={{backgroundColor: 'white'}}
      />
    );
  }
}
