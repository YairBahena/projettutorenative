import React, {Component} from 'react';
import {StyleSheet, View, TouchableOpacity, Text, ScrollView, FlatList, Image, ImageBackground } from 'react-native';
import Header from '../Header/Header';
import {RecetteButton} from '../Login/RecetteButton';
import {LikeButton} from '../Login/LikeButton';
import FireBase from '../FireBase';
import * as firebase from 'firebase';
import 'firebase/firestore';
import Icon from 'react-native-ionicons'

export default class Favoris extends Component{
  constructor(props){
    super(props)
    FireBase.init()
    this.state ={
      dataSource: [],
      dataTags: [],
      urifalse: require('../../images/favorite-heart-button.png')
    }
  }

  renderItem = ({item}) =>{
    return(
      <View style={{height: 232}}>
      <TouchableOpacity onPress={()=> this.props.navigation.navigate('Recette',{
         itemId: item.id,
         itemDoc: item.data(),
        })}>
      <ImageBackground style={{height: 200}}
        source={{uri: item.data().image}}>
        </ImageBackground>
      </TouchableOpacity>
        <View style={styles.buttonContainer}>
        <RecetteButton onPress={()=> this.props.navigation.navigate('Recette',{
           itemId: item.id,
           itemDoc: item.data(),
          })} style={styles.button}>
        <Text style={styles.titre}>
          {item.data().nom}
        </Text>
        </RecetteButton>
        <LikeButton onPress={()=> console.log('Aime')} style={styles.button}><Image
            source={this.state.urifalse} style={[styles.aimefalse, item.data().favorit ? styles.aimetrue : styles.aimefalse]}/></LikeButton>
         </View>
      </View>
    )
  }

  renderTags = ({item}) =>{
    return(
      <Text style={styles.welcome}>
         {item}
      </Text>
    )
  }

  componentDidMount(){
    this.getData();
  }
  getData(){
    const db = firebase.firestore();
    db.collection('recettes').where("favorit", "==", true).get().then(snapshot => {
      //snapshot.val() is the dictionary with all your keys/values from the '/store' path
      snapshot.docs.forEach(doc =>{
        this.setState({
          dataSource: [...this.state.dataSource, doc]
        })
      })

    })
  }
  render() {
    return (
      <View style={styles.container}>
        <Header/>
        <ScrollView style={styles.view}>
          <FlatList
            data={this.state.dataSource}
            renderItem={this.renderItem}
            keyExtractor={(item, index) => index.toString()}
          />
        </ScrollView>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  viewRecettes:{
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'center',
    height: 60,
    padding: 7,
  },
  aimetrue: {
    tintColor: 'black',
  },
  aimefalse: {
    tintColor: 'white',
  },
  headerLogo: {
    flex: 1,
    height: 100,
    resizeMode: 'contain'
  },
  container: {
    marginBottom: 60,
  },
  nom: {
    position: 'absolute',
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    margin: 10,
    fontWeight: 'bold',
    textShadowColor: 'black',
    textShadowOffset: {width: -2, height: 2},
    textShadowRadius: 5,
    backgroundColor: '#ED4C67',
  },
  text:{
    flex: 1,
    justifyContent: 'center',
  },
  buttonContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    textAlign: 'center',
    backgroundColor: '#ED4C67',
    borderColor: 'white',
    borderWidth: 1
  },
  button: {

  },
  titre:{
    color: 'white',
  },
  view:{

  }
});
