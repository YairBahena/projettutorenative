import React, {Component} from 'react';
import {StyleSheet, Text, View, FlatList, Image} from 'react-native';
import Header from '../Header/Header';

export default class Panier extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Header/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginBottom: 100
  },
  text: {
    color: '#545454',
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  }
});
