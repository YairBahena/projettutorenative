import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity, Image} from 'react-native';

const RecetteButton=({onPress, children})=>{
  return (
    <TouchableOpacity onPress={onPress} style={styles.buttonContainer}>
      {children}
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  buttonContainer: {
    width:'90%',
    alignItems: 'center',
    borderLeftColor: 'white',
    borderLeftWidth: 1
  }
});

export {RecetteButton};
