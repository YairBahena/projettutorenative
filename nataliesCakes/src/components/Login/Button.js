import React, {Component} from 'react';
import {StyleSheet, Text, View, FlatList, Image, TouchableOpacity} from 'react-native';
import Header from '../Header/Header';
import autoBind from 'react-autobind';

export default class Button extends Component {
  constructor(props){
    super(props)
    autoBind(this)
  }
  render() {
    return (
      <TouchableOpacity onPress={() => this.props.navigation.navigate('Recette',{ itemId: this.props.itemId})} style={styles.buttonContainer}>
      <Text>
      {this.props.itemId}
      </Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  buttonContainer: {
    width:'90%',
    alignItems: 'center',
    borderLeftColor: 'white',
    borderLeftWidth: 1
  }
});
