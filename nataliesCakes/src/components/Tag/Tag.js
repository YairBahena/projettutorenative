import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity, Text, View, FlatList, Image, ImageBackground } from 'react-native';
import Search from '../Search/Search';
import {RecetteButton} from '../Login/RecetteButton';
import {LikeButton} from '../Login/LikeButton';
import FireBase from '../FireBase';
import * as firebase from 'firebase';
import 'firebase/firestore';
import Icon from 'react-native-ionicons'

export default class Tag extends Component{
  constructor(props){
    super(props)
    FireBase.init()
    this.state ={
      dataSource: [],
      dataTags: [],
      urifalse: require('../../images/favorite-heart-button.png')
    }
  }

  renderItem = ({item}) =>{
    return(

        <FlatList
          data={item.data().tags}
          renderItem={this.renderTags}
          keyExtractor={(item, index) => index.toString()}
        />

    )
  }

  renderTags = ({item}) =>{
    return(
      <View style={styles.containerTags}>
      <Text style={styles.titles}>
      {item}
      </Text>
      </View>
    )
  }

  componentDidMount(){
    this.getData();
  }
  getData(){
    const db = firebase.firestore();
    db.collection('recettes').orderBy("date", "desc").get().then(snapshot => {
      //snapshot.val() is the dictionary with all your keys/values from the '/store' path
      snapshot.docs.forEach(doc =>{
        this.setState({
          dataSource: [...this.state.dataSource, doc],
          dataTags: [...this.state.dataTags, doc.data().tags]
        })
      })

    })
  }
  render() {
    return (
      <View style={styles.container}>
        <Search/>
        <View style={styles.view}>
          <FlatList
            data={this.state.dataSource}
            renderItem={this.renderItem}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  titles: {
    backgroundColor: '#ED4C67',
    flex: 1,
    color: 'white',
    fontSize: 14,
    alignSelf: 'flex-start',
    marginBottom: 5,
    marginTop: 5,
    marginLeft: 8,
    padding: 4,
    borderRadius:10,
    textAlign: 'center'
  },
  view:{
    width: '100%',
    marginBottom: 136,

  },
  containerTags:{
    flexDirection: 'row',
    alignItems: 'center',
    textAlign: 'center',
  }
});
