import React, {Component} from 'react';
import {Image, StyleSheet, Text, View, ScrollView, FlatList, ImageBackground, TouchableOpacity } from 'react-native';
import Header from '../Header/Header';
import FireBase from '../FireBase';
import * as firebase from 'firebase';
import LinearGradient from 'react-native-linear-gradient';

export default class Recette extends Component {
  constructor(props){
    super(props)

    this.state ={
      favoris: false
    }
  }

  renderTags = ({item}) =>{
    return(
      <Text style={styles.textTags}>
         {item}
      </Text>
    )
  }

  render() {
    const { navigation } = this.props;
    const itemDoc = navigation.getParam('itemDoc', 'error');
    return (
      <ScrollView style={styles.view}>
        <ImageBackground  style={{width: '100%', height: 200}} source={{uri: itemDoc.image}}>
        <LinearGradient
            colors={['black', '#ffffff00', '#ffffff00']}
            style={styles.contentContainer}
          >
        </LinearGradient>
        <Text style={styles.textNom}>{itemDoc.nom.toUpperCase()}</Text>
        <View style={{flexDirection: 'row'}}>
           <Image source={require('../../images/gateau_parts.png')} style={styles.imagesPersonnes}/>
           <Text style={styles.textPersonnes}>{itemDoc.nombrePersonnes} personnes</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
           <Image source={require('../../images/four.png')} style={styles.imagesTemps}/>
           <Text style={styles.textPersonnes}>{itemDoc.tempsCuisson} minutes</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
           <Image source={require('../../images/outil.png')} style={styles.imagesTempsCuisson}/>
           <Text style={styles.textPersonnes}>{itemDoc.tempsCuisson} minutes</Text>
        </View>
        </ImageBackground >
        <Text style={styles.titles}>INGRÉDIENTS</Text>
        <View style={styles.buttonContainer}>
        <FlatList
          data={itemDoc.ingredients}
          renderItem={this.renderTags}
          keyExtractor={(item, index) => index.toString()}
        />
        </View>
        <Text style={styles.titles}>MATERIÉL</Text>
        <FlatList
          data={itemDoc.materiels}
          renderItem={this.renderTags}
          keyExtractor={(item, index) => index.toString()}
        />
        <Text style={styles.titles}>PRÉPARATION</Text>
        <Text style={styles.text}>{itemDoc.preparation}</Text>

        <Text style={styles.titles}>DIFICULTÉ</Text>
        <Text style={styles.text}>{itemDoc.dificulte}</Text>

        <Text style={styles.titles}>COMMENTAIRES</Text>
        <Text style={styles.text}>{itemDoc.commentaire}</Text>
        <TouchableOpacity style={styles.likeContainer}>
        <Image source={require('../../images/favorite-heart-button.png')}  style={[styles.aimefalse, itemDoc.favorit ? styles.aimetrue : styles.aimefalse]}/>
        </TouchableOpacity>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  flatlistcontainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    textAlign: 'center',
    backgroundColor: '#ED4C67',
    borderColor: 'black',
    borderWidth: 1
  },
  likeContainer:{
    width: '100%',
    backgroundColor: '#ED4C67',
    textAlign: 'center',
    alignItems: 'center'
  },
  aimetrue: {
    width: 30,
    height: 30,
    tintColor: 'black',
    borderRadius: 10,
  },
  aimefalse: {
    width: 30,
    height: 30,
    tintColor: 'white',
    borderRadius: 10,
  },
  imagesPersonnes:{
    width: 40,
    height: 40,
    tintColor: 'black',
    marginRight: 4,
    marginLeft: 10,
  },
  imagesTemps:{
    width: 40,
    height: 40,
    tintColor: 'black',
    marginRight: 4,
    marginLeft: 10,

  },
  imagesTempsCuisson:{
    width: 40,
    height: 40,
    tintColor: 'black',
    marginRight: 4,
    marginLeft: 10,

  },
  textPersonnes:{
    color: 'white',
    fontSize: 15,
    alignSelf: 'flex-start',
    backgroundColor: 'black',
    borderRadius: 10,
    padding: 2,
    marginTop: 8,
  },
  textTags: {
    flex: 1,
    color: 'white',
    fontSize: 14,
    backgroundColor: 'black',
    alignSelf: 'flex-start',
    marginBottom: 5,
    marginTop: 5,
    marginLeft: 8,
    padding: 4,
    borderRadius:10,
  },
  titles: {
    width: '100%',
    backgroundColor: '#ED4C67',
    color: 'white',
    fontSize: 20,
    textAlign: 'center'
  },
  contentContainer: {
    flex : 1,
    paddingTop: 50,
    paddingHorizontal: 20,
    paddingVertical: 20,
    overflow:'visible',
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  container: {
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    textAlign: 'center',
  },
  text: {
    color: 'black',
    fontSize: 14,
    textAlign: 'left',
    flex: 1,
    alignSelf: 'flex-start',
    marginBottom: 5,
    marginTop: 5,
    marginLeft: 8,
  },
  textNom: {
    color: 'white',
    fontSize: 22,
    textAlign: 'left',
    margin: 10,
    position: 'absolute',
    backgroundColor: 'black',
    padding: 4,
    borderRadius:10,
  },
  submitButtonStyle: {
   paddingTop:1,
   paddingBottom:1,
   backgroundColor:'black',
   borderRadius:10,
   borderWidth: 1,
   borderColor: 'blue',

 },
 view:{
   backgroundColor: '#FDEEF0'
 }
});
