import React, {Component} from 'react';
import {StyleSheet, Text, View, FlatList, Image} from 'react-native';

export default class Header extends Component {
  render() {
    return (
      <View style={styles.header}>
        <Image
        style={styles.headerLogo}
        source={require('../../images/Logo.png')}>
        </Image>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  header: {
    alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'center',
    height: 60,
    padding: 7,
  },
  headerLogo: {
    flex: 1,
    height: 100,
    resizeMode: 'contain'
  }
});
